package be.kdg.java2;

import com.afrunt.randomjoke.Jokes;

public class Person {
    public void tellJoke() {
        Jokes jokes = new Jokes()
                .withDefaultSuppliers();

        jokes.randomJoke().ifPresent(joke -> System.out.println(joke.getText()));
    }
}
